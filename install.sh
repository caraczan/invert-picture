#!/bin/bash

# descriptivly what script does
#  
# invert.py -> /opt/InvertPictureProgram/invert
# /opt/InvertPictureProgram/invert -symlink-> /bin

### functions

installing() {
	# creates folder "InvertPictureProgram" in /opt folder 
	sudo mkdir --parents /opt/InvertPictureProgram;

	# puts invert.py to /opt/InvertPictureProgram/
	# and renames to invert to make it command-like
	sudo cp --update invert.py /opt/InvertPictureProgram/invert.py

	# creates symbolic link (weak link) from invert.py file to /bin/invert
	if [ /bin/invert ]; then
		sudo rm /bin/invert
	fi
	sudo ln --symbolic /opt/InvertPictureProgram/invert.py /bin/invert  
}

check_n_install_required() {

	# checking python version
	pver=$(python3 -V | cut -d' ' -f2)
	temp=$(echo $pver | cut -d. -f2)
	pver=$(echo $(echo $pver | cut -d. -f1)'.'$temp)

	# checking python3 from homebrew
	if [[ "$pver" < "3.0" ]]; then
		printf "Python, present version %s - required Python >= 3.0\n" $pver
		flag_python=0
	else
		printf "Python present, version %s - ok\n" $pver
		flag_python=1
	fi

	# checking if magick exist
	if [[ "$(command -v magick)" =~ "magick not found" ]]; then
		printf "imagemagick - required\n"
		flag_magick=0
	else
		printf "Imagemagick present\n"
		flag_magick=1
	fi

	
	if [[ "$OSTYPE" == "darwin"* ]]; then # darwin/MacOS
		# for mac
		# installing homebrew
		if [[ "$(command -v brew)" =~ "brew not found" ]]; then
			echo "homebrew - installing"
			/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
		else
			echo "homebrew - ok"
		fi
		# installing python 3 if its non existent
		if [[ flag_magick == 0 ]]; then
			echo "Python 3 - installing"
			brew install python3
		fi
		# installing imagegick from homebrew
		if [[ flag_magick == 0 ]]; then
			echo "Imagemagick - installing"
			brew install imagemagick
		fi
	else # gnu-linux 
		lin_ver=$(cat /etc/os-release | grep ID_LIKE | cut -d= -f2)
		
		if [[ flag_python == 0 ]]; then
			
			echo "Python 3 - installing"
			if [[ $lin_ver == "arch" ]]; then
				sudo pacman -S python3
			elif [[ $lin_ver == "debian" ]]; then
				sudo apt install python3
			else
				echo "Unrecognized, as of yet Linux, please install python with your packet manager, and send us report on gitlab"
			fi

		fi

		if [[ flag_magick == 0 ]]; then
			
			echo "Imagemagick - installing"
			if [[ $lin_ver == "arch" ]]; then
				sudo pacman -S imagemagick
			elif [[ $lin_ver == "debian" ]]; then
				sudo apt install imagemagick
			else
				echo "Unrecognized, as of yet Linux, please install imagemagick with your packet manager, and send us report on gitlab"
			fi

		fi
		
	fi
}

### body

check_n_install_required
installing