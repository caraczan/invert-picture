[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Open Source Love svg1](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![Bash Shell](https://badges.frapsoft.com/bash/v1/bash.png?v=103)](https://github.com/ellerbrock/open-source-badges/)

# InvertPicture
invert is a simply script/command that inverts pictures colors.

# Instalation
**Required Dependencies:**
1. [Python](https://www.python.org)
2. [ImageMagick](https://imagemagick.org/index.php)

## Script Intallation

I've created script `install.sh` that will detect and install required/missing  dependencies.

Just run it `./install.sh`.

`note:` if for some reason `install.sh` is not executable add proper right to it with `sudo chmod +x install.sh`. 

## Manual Installation

On Macintosh.
To install newer python on mac you have two way, best way is to first download homebrew and install python from there, or you can just download direclty from python page link above.
Imagemagick similarly has two ways of installation first from homebrew second from website.

Macintosh will ask for password to your machine.

0. Paste line below to Terminal and run it, you will be prompted to give your password.
```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```
1. Now paste this line to install python 3 on your mac.
```brew install python3```
this will install python 3 and all its dependencies.
2. Now paste following line
```brew install imagemagick```
this will install imagemagick and all its dependencies.

On Linux.
Python version 3+ should be on every linux distribution.
The only thing left is to install imagemagick.
* Arch/Arch-based 	: `pacman -S imagemagick or paru/yay -S imagemagick`
* Solus				: `eopkg install imagemagick`
* Debian			: `apt install python`
* Ubuntu			: `apt install python3`
* Fedora/RHEL?		: `dnf install python`

# How does it work?
1. In terminal go to directory with pictures you want to invert,
2. Use `invert` 
3. Check new pics in newly created folder.

# Flags
* ```-e .ext1 .ext2 ..``` - takes multiply *.extensionName* (e.g. .png .tiff ..), it replaces standard list of extension with one given after flag. Useful with -p options if file given there has non-standard extension.
* ```-ae .ext1 .ext2 ..``` - takes multiply and append on top of standard extenion, it will not remember after action.
* ```-dn directory_name``` - name of directory that script will make and put or put inverted pictures.
* ```-p picture.ext``` - name of the picture to invert, it will invert just this picture. If you want to invert picture with non-standard extension use combination of `-e` flag.

## Standard extensions
- .png
- .jpg/jpeg
- .gif
- .tiff

---

### Now (27.03.2021)
Script received massive rework. Now it will invert and save new pictures directory where script was called. Installer(still WIP on some platforms) copies `invert.py` to `/opt/InvertPictureProgram/` and links it `/bin/` as invert. So now you can call it from any place in your linux system. 

Installer still lacks support for some distributions but it's on the good tracks.

---

### Previously
Script has been flattened and simpliefied for sake of clarity and overall speed. Now there is only one way script invert, ImageMagick. Magick is marginally slower than Pillow/PIL but impact is marginal. Script no longer needlesly check things constantly.

---
